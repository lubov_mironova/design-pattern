package ru.mla.iceCreamCafe.decorators;

import ru.mla.iceCreamCafe.object.Component;

public abstract class IceCreamDecorator implements Component {
    private Component component;

    public IceCreamDecorator(Component component) {
        this.component = component;
    }

    @Override
    public void topping() {
        component.topping();
        afterTopping();
    }

    public abstract void afterTopping();
}
