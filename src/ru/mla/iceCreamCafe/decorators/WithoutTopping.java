package ru.mla.iceCreamCafe.decorators;

import ru.mla.iceCreamCafe.object.Component;

public class WithoutTopping extends IceCreamDecorator {
    public WithoutTopping(Component component) {
        super(component);
    }

    @Override
    public void afterTopping() {
        System.out.println(" - без топпинга");
    }
}