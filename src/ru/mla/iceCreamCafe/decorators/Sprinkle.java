package ru.mla.iceCreamCafe.decorators;

import ru.mla.iceCreamCafe.object.Component;

public class Sprinkle extends IceCreamDecorator {
    public Sprinkle(Component component) {
        super(component);
    }

    @Override
    public void afterTopping() {
        System.out.println(" - добавлена посыпка");
    }
}
