package ru.mla.iceCreamCafe.decorators;

import ru.mla.iceCreamCafe.object.Component;

public class Syrup extends IceCreamDecorator {
    public Syrup(Component component) {
        super(component);
    }

    @Override
    public void afterTopping() {
        System.out.println(" - добавлен сироп");
    }
}