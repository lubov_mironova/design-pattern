package ru.mla.iceCreamCafe;

import ru.mla.iceCreamCafe.decorators.Sprinkle;
import ru.mla.iceCreamCafe.decorators.Syrup;
import ru.mla.iceCreamCafe.decorators.WithoutTopping;
import ru.mla.iceCreamCafe.object.Component;
import ru.mla.iceCreamCafe.object.IceCream;
import ru.mla.iceCreamCafe.object.Smoothie;
import ru.mla.iceCreamCafe.object.Yoguгt;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Component iceCream;
        Component smoothie;
        Component yogurt;
        Random random = new Random();
        boolean showBorder = random.nextBoolean();
        if (showBorder) {
            iceCream = new Sprinkle(new IceCream());
            smoothie = new Syrup(new Smoothie());
            yogurt = new WithoutTopping(new Yoguгt());
        } else {
            iceCream = new IceCream();
            smoothie = new Smoothie();
            yogurt = new Yoguгt();
        }

        iceCream.topping();
        smoothie.topping();
        yogurt.topping();

        iceCream = new Sprinkle(new Syrup(new IceCream()));
        iceCream.topping();
    }
}