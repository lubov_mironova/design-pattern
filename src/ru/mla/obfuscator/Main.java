package ru.mla.obfuscator;

import java.util.Scanner;

/**
 * Класс для создания шпаргалки в маршрутку
 *
 * @author Mironova L.
 */
public class Main {

    public Main() {
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); //TODO:
//проверка
        double price, site;
        double cost = 0;
/*
проверка номер два
 */
        System.out.print("Введите цену= ");
        price = scanner.nextInt(); //цена
        System.out.print("Введите вместимость= ");
        site = scanner.nextInt(); //стоимость

        if (price < 0 || price > 500 || site < 0 || site > 60) {
            System.out.println("Неверные данные");
            return;
        }

        for (int x = 1; x <= site; x++) {
            cost += price;
            System.out.printf("%d чел. - %.0f руб. \n", x, cost);
        }
    }
}