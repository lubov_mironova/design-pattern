package ru.mla.obfuscator;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Фасад для обфускатора
 */
class FacadeObfuscate {
    private String startWay;
    private String finWay;

    public FacadeObfuscate(String startWay, String finWay) {
        this.startWay = startWay;
        this.finWay = finWay;
    }

    void obfuscate() throws IOException {
        String listLine = fileReader(startWay);

        String multiLine = deleteComment(deleteGaps(listLine));

        String startFileName = getFileName(startWay);
        String finFileName = getFileName(finWay);

        String renameMultiline = replaceFileName(multiLine, startFileName, finFileName);

        print(renameMultiline, finWay);
    }

    /**
     * Считывает файл по указанному пути
     *
     * @param startWay начальный путь к файлу
     * @return файл в мультистроке
     */
    private static String fileReader(String startWay) {
        StringBuilder listLine = new StringBuilder();
        try {
            List<String> list = Files.readAllLines(Paths.get(startWay));
            //получение мультистроки
            for (String s : list) {
                listLine.append(s); //listLine += s
            }
        } catch (IOException e) {
            System.out.println("not found");
        }
        return listLine.toString();
    }

    /**
     * Удаляет комментарии
     *
     * @param listLine файл в мультистроке
     * @return мультистрока без комментариев
     */
    private static String deleteComment(String listLine) {
        return listLine.replaceAll("(/\\*.+?\\*/)|(//.+?)[:;a-zA-Zа-яА-ЯЁё]*", "");
    }


    /**
     * Меняет имя класса в файле
     *
     * @param multiLine     мультистрока без комментариев
     * @param startFileName начальное имя файла
     * @param finFileWay    конечное имя файла
     * @return измененная строка с измененным именем класса в мультисроке
     */
    private static String replaceFileName(String multiLine, String startFileName, String finFileWay) {
        return multiLine.replaceAll(startFileName, finFileWay);
    }

    /**
     * Удаляет лишние пробелы
     *
     * @param listLine мультистрока без комментариев
     * @return очищенная мультистрока
     */
    private static String deleteGaps(String listLine) {
        return listLine.replaceAll("\\s+(?![^\\d\\s])", "");
    }

    /**
     * Получает имя из пути файла без расширения
     *
     * @param fileWay путь файла
     * @return файл без расширения
     */
    private static String getFileName(String fileWay) {
        Path p = Paths.get(fileWay);
        String fileName = p.getFileName().toString();
        return fileName.replaceAll("\\..*", "");
    }

    /**
     * Записывает мультистроку в файл
     *
     * @param renameMultiline мультистрока с измененным Main
     * @param newFileFullWay  путь к файлу с новым именем
     * @throws IOException ошибка
     */
    private static void print(String renameMultiline, String newFileFullWay) throws IOException {
        try (FileWriter writer = new FileWriter(newFileFullWay)) {
            writer.write(renameMultiline);
        }
    }
}