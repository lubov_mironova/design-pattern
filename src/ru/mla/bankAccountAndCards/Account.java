package ru.mla.bankAccountAndCards;

/**
 * Класс для работы с аккаунтами
 *
 * @author Mironova L.
 */
@SuppressWarnings("unused")
public class Account {
    private final String number;
    private final String owner;
    private int amount;

    public Account(final String number, final String owner) {
        this.number = number;
        this.owner = owner;
    }

    public Account(String number, String owner, int amount) {
        this.number = number;
        this.owner = owner;
        this.amount = amount;
    }

    public String getNumber() {
        return number;
    }

    public String getOwner() {
        return owner;
    }

    public int getAmount() {
        return amount;
    }

    /**
     * Снимает деньги с карты
     *
     * @param amountToWithdraw сумма для снятия
     * @return 0 или баланс карты после снятия
     */
    private int withdraw(int amountToWithdraw) {
        if (amountToWithdraw < 0) {
            return 0;
        }
        if (amountToWithdraw > amount) {
            final int amountToReturn = amount;
            amount = 0;
            return amountToReturn;
        } else {
            amount = amount - amountToWithdraw;
            return amountToWithdraw;
        }
    }

    /**
     * Пополняет баланс карты
     *
     * @param money сумма для пополнения
     * @return 0 или баланс карты после пополнения
     */
    private int deposit(int money) {
        if (money <= 0) {
            return 0;
        } else {
            amount += money;
            return money;
        }
    }

    /**
     * Класс для представления карт
     */
    public class Card {
        private final String number;

        public Card(final String number) {
            this.number = number;
        }

        public String getNumber() {
            return number;
        }

        public int withdraw(final int amountToWithdraw) {
            return Account.this.withdraw(amountToWithdraw);
        }

        public int deposit(int money) {
            return Account.this.deposit(money);
        }
    }
}