package ru.mla.bankAccountAndCards;

public interface Message {
    String CASH_WITHDRAWAL_MESSAGE = "Какую сумму хотите снять?";
    String CASH_DEPOSIT_MESSAGE = "Какую сумму хотите положить?";
    String ERROR_MESSAGE = "Введенная сумма больше, чем баланс карты, вы можете снять: ";
    String INCORRECT_DATA = "Некорректные данные";
    String SELECT_ACCOUNT = "Выберите аккаунт:\n1)Сбербанк\n2)ВТБ\nВведите число: ";
    String SELECT_ACTION = "\nВыберите действие:\n1)Снять деньги\n2)Положить деньги\n3)Баланс\n4)Отмена\nВведите число: ";
    String QUESTION_ABOUT_ACCOUNT = "\nВернуться к выбору аккаунта?\n1)Да\n2)Нет\nВведите число: ";
    String QUESTION_ABOUT_ACTION = "\nВернуться к выбору действия?\n1)Да\n2)Нет\nВведите число: ";
    String THANKS = "Спасибо за визит!";

}
