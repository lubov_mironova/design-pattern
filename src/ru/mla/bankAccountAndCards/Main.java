package ru.mla.bankAccountAndCards;

import java.util.Scanner;

/**
 * Класс для работы с банковскими аккаунтами
 *
 * @author Mironova L.
 */
@SuppressWarnings("unused")
public class Main implements Message{

    public static void main(String[] args) {
        final Account sberbank = new Account("1234567812345678", "People", 5800);
        final Account vtb = new Account("8765432187654321", "People", 8300);

        final Account.Card mir = sberbank.new Card("5982 5498 4987 0356");
        final Account.Card mastercard = sberbank.new Card("9547 2168 3168 3216");
        final Account.Card maestro = vtb.new Card("9515 4987 0357 6549");
        do {
            int account = messageOutputWithSavedValues(SELECT_ACCOUNT);
            if (account == 1) {
                do {
                    performsAction(sberbank, mastercard);
                } while (messageOutputWithSavedValues(QUESTION_ABOUT_ACTION) == 1);
            }
            if (account == 2) {
                do {
                    performsAction(vtb, maestro);
                } while (messageOutputWithSavedValues(QUESTION_ABOUT_ACTION) == 1);
            }
        } while (messageOutputWithSavedValues(QUESTION_ABOUT_ACCOUNT) == 1);
        if (messageOutputWithSavedValues(QUESTION_ABOUT_ACCOUNT) == 2) {
            messageOutput(THANKS);
        }
    }

    /**
     * Производит действия в соответствии с выбором пользователя
     *
     * @param account аккаунт, к которому прикреплена карта
     * @param card    карта, над которой совершаются действия
     */
    public static void performsAction(Account account, Account.Card card) {
        int choice = messageOutputWithSavedValues(SELECT_ACTION);
        balanceOutput(account);
        if (choice == 1) {
            int withdraw = messageOutputWithSavedValues(CASH_WITHDRAWAL_MESSAGE);
            int num = cashWithdrawal(withdraw, card, account);
            if (num == -1) {
                System.out.println(ERROR_MESSAGE + account.getAmount());
            }
            if (num != -1) {
                balanceOutput(account);
            }
        }

        if (choice == 2) {
            int deposit = messageOutputWithSavedValues(CASH_DEPOSIT_MESSAGE);
            replenishment(deposit, card);
            balanceOutput(account);
        }
        if (choice == 3 || choice == 4) {
            balanceOutput(account);
        }
        if (choice != 1 && choice != 2 && choice != 3 && choice != 4) {
            messageOutput(INCORRECT_DATA);
        }
    }

    /**
     * Выводит сообщение пользователю и сохраняет введенное число
     *
     * @return введенное с консоли число
     */
    private static int messageOutputWithSavedValues(String message) {
        Scanner scanner = new Scanner(System.in);
        System.out.print(message);
        return scanner.nextInt();
    }

    /**
     * Снимает деньги с баланса
     *
     * @param withdraw сумма снятия
     * @param card     карта, счет которой необходимо пополнить
     * @param account  аккаунта, на который записана карта, счет которой необходимо пополнить
     * @return -1, если баланс счета меньше суммы снятия и баланс после снятия, если сумма снятия больше нуля и меньше баланса счета
     */
    private static int cashWithdrawal(int withdraw, Account.Card card, Account account) {
        int num = card.withdraw(withdraw);
        if (num > account.getAmount()) {
            return -1;
        }
        if (num == 0) {
            messageOutput(INCORRECT_DATA);
        }
        return num;
    }

    /**
     * Пополняет баланс карты
     *
     * @param deposit сумма пополнения
     * @param card    карта, счет которой необходимо пополнить
     */
    private static void replenishment(int deposit, Account.Card card) {
        int num = card.deposit(deposit);
        if (num == 0) {
            messageOutput(INCORRECT_DATA);
        }
    }

    /**
     * Выводит баланс
     *
     * @param account аккаунта, на который записана карта, баланс которой необходимо вывести
     */
    private static void balanceOutput(Account account) {
        System.out.println("Баланс: " + account.getAmount());
    }

    /**
     * Выводит пользователю разнообразные сообщения
     *
     * @param message сообщение для пользователя
     */
    private static void messageOutput(String message) {
        System.out.println(message);
    }
}